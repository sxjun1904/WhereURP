package com.whereta.service.impl;

import com.whereta.dao.IAccountRoleDao;
import com.whereta.dao.IPermissionDao;
import com.whereta.dao.IRoleDao;
import com.whereta.dao.IRolePermissionDao;
import com.whereta.model.AccountRole;
import com.whereta.model.Permission;
import com.whereta.model.Role;
import com.whereta.model.RolePermission;
import com.whereta.service.IRoleService;
import com.whereta.vo.ResultVO;
import com.whereta.vo.RoleCreateVO;
import com.whereta.vo.RoleEditVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Vincent
 * @time 2015/9/1 16:36
 */
@Service("roleService")
public class RoleServiceImpl implements IRoleService {
    @Resource
    private IRoleDao roleDao;
    @Resource
    private IAccountRoleDao accountRoleDao;
    @Resource
    private IRolePermissionDao rolePermissionDao;
    @Resource
    private IPermissionDao permissionDao;

    /**
     * 获取显示的角色
     *
     * @return
     */
    public ResultVO getShowRoles(int userId) {
        ResultVO resultVO = new ResultVO(true);

        Set<Integer> roleIdSet = accountRoleDao.selectRoleIdSet(userId);

        //获取所有角色
        List<Role> roles = roleDao.selectAll();
        //我的角色
        List<Role> myRoles = new ArrayList<Role>();

        for (Integer id : roleIdSet) {
            Role role = roleDao.get(roles, id);
            if (role != null) {
                myRoles.add(role);
            }
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("rows", myRoles);
        resultVO.setData(map);
        return resultVO;
    }

    private static boolean isKeyExist(List<Role> roles,String key){
        for(Role role:roles){
            if(role.getKey().equals(key)){
                return true;
            }
        }
        return false;
    }

    /**
     * 创建角色
     *
     * @param createVO
     * @return
     */
    public ResultVO createRole(RoleCreateVO createVO, int userId) {
        ResultVO resultVO = new ResultVO(true);
        List<Role> roleList = roleDao.selectAll();
        if (isKeyExist(roleList,createVO.getKey())) {
            resultVO.setOk(false);
            resultVO.setMsg("角色键值已存在");
            return resultVO;
        }
        Role role = new Role();
        role.setName(createVO.getName());
        role.setKey(createVO.getKey());
        roleDao.createRole(role);
        //添加账号角色关联
        AccountRole accountRole = new AccountRole();
        accountRole.setAccountId(userId);
        accountRole.setRoleId(role.getId());
        accountRoleDao.createAccountRole(accountRole);
        resultVO.setMsg("创建角色成功");
        return resultVO;
    }

    /**
     * 删除角色
     *
     * @param roleId
     * @return
     */
    public ResultVO deleteRole(int roleId) {
        ResultVO resultVO = new ResultVO(true);
        //获取所有角色
        List<Role> roles = roleDao.selectAll();
        Role role = roleDao.get(roles, roleId);
        if (role == null) {
            resultVO.setOk(false);
            resultVO.setMsg("角色不存在");
            return resultVO;
        }
        roleDao.deleteRole(roleId);
        rolePermissionDao.deleteByRoleId(roleId);
        accountRoleDao.deleteByRoleId(roleId);
        resultVO.setMsg("删除角色成功");
        return resultVO;
    }

    /**
     * 编辑角色
     *
     * @param editVO
     * @return
     */
    public ResultVO editRole(RoleEditVO editVO) {
        ResultVO resultVO = new ResultVO(true);
        //获取所有角色
        List<Role> roles = roleDao.selectAll();
        Role role = roleDao.get(roles, editVO.getId());
        if (role == null) {
            resultVO.setOk(false);
            resultVO.setMsg("角色不存在");
            return resultVO;
        }
        if (!role.getKey().equals(editVO.getKey()) && isKeyExist(roles,editVO.getKey())) {
            resultVO.setOk(false);
            resultVO.setMsg("角色键值已存在");
            return resultVO;
        }
        Role update = new Role();
        update.setId(editVO.getId());
        update.setKey(editVO.getKey());
        update.setName(editVO.getName());
        int num = roleDao.updateRole(update);
        resultVO.setMsg("编辑角色成功");
        return resultVO;
    }

    /**
     * 角色授权
     *
     * @param roleId
     * @param perIdArray
     * @return
     */
    public ResultVO grantPermissions(int roleId, Integer[] perIdArray) {

        ResultVO resultVO = new ResultVO(true);
        //获取所有角色
        List<Role> roles = roleDao.selectAll();
        //获取角色
        Role role = roleDao.get(roles, roleId);
        if (role == null) {
            resultVO.setOk(false);
            resultVO.setMsg("角色不存在");
            return resultVO;
        }

        //获取所有权限
        List<Permission> permissions = permissionDao.selectAll();

        Set<Integer> perIdSet = new HashSet<Integer>();

        for (Integer id : perIdArray) {
            perIdSet.add(id);
   /*         List<Integer> childrenPermissionIds = PermissionServiceImpl.getChildrenPermissionIds(id, permissions);
            perIdSet.addAll(childrenPermissionIds);*/
        }

        //删除角色权限
        rolePermissionDao.deleteByRoleId(roleId);
        //授权
        if (!perIdSet.isEmpty()) {
            for (Integer perId : perIdSet) {
                RolePermission rolePermission = new RolePermission();
                rolePermission.setPermissionId(perId);
                rolePermission.setRoleId(roleId);
                rolePermissionDao.addRolePermission(rolePermission);

            }
        }
        resultVO.setMsg("授权成功");
        return resultVO;
    }
}
