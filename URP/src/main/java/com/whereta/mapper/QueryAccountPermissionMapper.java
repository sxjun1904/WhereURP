package com.whereta.mapper;

import com.whereta.model.QueryAccountPermission;
import org.springframework.stereotype.Component;

@Component("queryAccountPermissionMapper")
public interface QueryAccountPermissionMapper {
    int deleteByPrimaryKey(Integer accountId);

    int insert(QueryAccountPermission record);

    int insertSelective(QueryAccountPermission record);

    QueryAccountPermission selectByPrimaryKey(Integer accountId);

    int updateByPrimaryKeySelective(QueryAccountPermission record);

    int updateByPrimaryKeyWithBLOBs(QueryAccountPermission record);
}